import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './modules/login/login.component';
import { MixingComponent } from './modules/mixing/mixing.component';
import { OnlineComponent } from './modules/online/online.component';
import { MasteringComponent } from './modules/mastering/mastering.component';
import { MasteringOnlineComponent } from './modules/mastering_online/mastering-online.component';
import { AnalogComponent } from './modules/analog/analog.component';
import { TuningComponent } from './modules/tuning/tuning.component';
import { AdditionComponent } from './modules/addition/addition.component';
import { FormatComponent } from './modules/format/format.component';
import { UrgencyComponent } from './modules/urgency/urgency.component';
import { SoundEngineerComponent } from './modules/sound_engineer/sound-engineer.component';
import { PaymentComponent } from './modules/payment/payment.component';
import { RecommendationComponent } from './modules/recommendation/recommendation.component';
import { WishesComponent } from './modules/wishes/wishes.component';
import { ContactsComponent } from './modules/contacts/contacts.component';
import { HeaderComponent } from "./modules/header/header.component";
import { ServicesComponent } from "./modules/services/finalPages.services";
import { NgxMaskModule } from 'ngx-mask';
import { SendingComponent } from './modules/sending/sending.component';
import { FinalComponent } from './modules/final/final.component';
import { RepeatComponent } from './modules/repeat/repeat.component';
import { FormsModule } from "@angular/forms";
import { TotalComponent } from './modules/total/total.component';
import { BurgerMenuComponent } from './modules/shired/burger-menu/burger-menu.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MixingComponent,
    OnlineComponent,
    MasteringComponent,
    MasteringOnlineComponent,
    AnalogComponent,
    TuningComponent,
    AdditionComponent,
    FormatComponent,
    UrgencyComponent,
    SoundEngineerComponent,
    PaymentComponent,
    RecommendationComponent,
    WishesComponent,
    HeaderComponent,
    ContactsComponent,
    SendingComponent,
    FinalComponent,
    RepeatComponent,
    TotalComponent,
    BurgerMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxMaskModule.forRoot(),
    FormsModule
  ],
  providers: [ServicesComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
