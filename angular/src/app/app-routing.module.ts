import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MixingComponent} from "./modules/mixing/mixing.component";
import {LoginComponent} from "./modules/login/login.component";
import {OnlineComponent} from "./modules/online/online.component";
import {MasteringComponent} from './modules/mastering/mastering.component';
import {MasteringOnlineComponent} from './modules/mastering_online/mastering-online.component';
import {AnalogComponent} from './modules/analog/analog.component';
import {TuningComponent} from './modules/tuning/tuning.component';
import {AdditionComponent} from './modules/addition/addition.component';
import {FormatComponent} from './modules/format/format.component';
import {UrgencyComponent} from './modules/urgency/urgency.component';
import {SoundEngineerComponent} from './modules/sound_engineer/sound-engineer.component';
import {PaymentComponent} from './modules/payment/payment.component';
import {RecommendationComponent} from './modules/recommendation/recommendation.component';
import {WishesComponent} from './modules/wishes/wishes.component';
import {ContactsComponent} from "./modules/contacts/contacts.component";
import {SendingComponent} from './modules/sending/sending.component';
import {FinalComponent} from './modules/final/final.component';
import {RepeatComponent} from './modules/repeat/repeat.component';
import {TotalComponent} from './modules/total/total.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'mixing', component: MixingComponent },
  { path: 'online', component: OnlineComponent },
  { path: 'mastering', component: MasteringComponent },
  { path: 'masteringOnline', component: MasteringOnlineComponent },
  { path: 'analog', component: AnalogComponent },
  { path: 'tuning', component: TuningComponent },
  { path: 'addition', component: AdditionComponent },
  { path: 'format', component: FormatComponent },
  { path: 'urgency', component: UrgencyComponent },
  { path: 'soundEngineer', component: SoundEngineerComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'recommendation', component: RecommendationComponent },
  { path: 'wishes', component: WishesComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'sending', component: SendingComponent },
  { path: 'final', component: FinalComponent },
  { path: 'repeat', component: RepeatComponent },
  { path: 'total', component: TotalComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
