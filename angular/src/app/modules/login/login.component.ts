import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
  }

  email = '';

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  onSubmit() {
    this.service.emailManager=this.email;
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,./;'[]\=-)(*&^%$#@!~`";
    const lengthOfCode = 40;

    function makeRandom(lengthOfCode: number, possible: string) {
      let text = "";
      for (let i = 0; i < lengthOfCode; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    this.service.key =  makeRandom(lengthOfCode, possible);
    this.service.emailManager=this.email;
    localStorage.setItem('key',this.service.key);
    localStorage.setItem('login',this.service.emailManager);
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/contacts']);
  }


  enterEmail(event: string) {
    this.email = event;
  }
}
