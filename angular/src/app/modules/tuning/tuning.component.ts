import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {mixContainer, ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-tuning',
  templateUrl: './tuning.component.html',
  styleUrls: ['./tuning.component.css']
})
export class TuningComponent implements OnInit {


  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined || this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if (this.service.artist.length === 0) {
      this.router.navigate(['/contacts']);
      return;
    }
  }

  entity: boolean;
  withoutTeacher = 0;
  fromTeacher = 0;
  extraVoice = 0;
  online = 0;
  selectedExtraVoice: boolean;
  costExtraVoice: string;
  nameExtraVoice: string;
  indexExtraVoice: number;
  deadlineExtraVoice: string;
  selectedWithoutTeacher: boolean;
  costWithoutTeacher: string;
  nameWithoutTeacher: string;
  indexWithoutTeacher: number;
  deadlineWithoutTeacher: string;
  selectedFromTeacher: boolean;
  costFromTeacher: string;
  nameFromTeacher: string;
  indexFromTeacher: number;
  deadlineFromTeacher: string;
  selectedOnline: boolean;
  costOnline: string;
  nameOnline: string;
  indexOnline: number;
  deadlineOnline: string;

  ngOnInit() {
    this.entity = this.service.entity;
    this.service.mixContainers.forEach((container, key) => {
      if (container.name.includes("Без педагога")) {
        this.selectedWithoutTeacher = true;
        this.indexWithoutTeacher = key;
        this.nameWithoutTeacher = container.name;
        this.costWithoutTeacher = container.cost;
        this.deadlineWithoutTeacher = container.deadline;
        this.withoutTeacher = container.count;
      }
      if (container.name.includes("С педагогом")) {
        this.selectedFromTeacher = true;
        this.indexFromTeacher = key;
        this.nameFromTeacher = container.name;
        this.costFromTeacher = container.cost;
        this.deadlineFromTeacher = container.deadline;
        this.fromTeacher = container.count;
      }
      if (container.name.includes("Доп. голос")) {
        this.selectedExtraVoice = true;
        this.indexExtraVoice = key;
        this.nameExtraVoice = container.name;
        this.costExtraVoice = container.cost;
        this.deadlineExtraVoice = container.deadline;
        this.extraVoice = container.count;
      }
      if (container.name.includes("Онлайн")) {
        this.selectedOnline = true;
        this.indexOnline = key;
        this.nameOnline = container.name;
        this.costOnline = container.cost;
        this.deadlineOnline = container.deadline;
        this.online = container.count;
      }
    });
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goAddition() {
    this.router.navigate(['/addition']);
  }

  goAnalog() {
    this.router.navigate(['/analog']);
  }

  next() {
    if (this.extraVoice > 0) {
      if (this.selectedExtraVoice) {
        this.service.mixContainers[this.indexExtraVoice] = new mixContainer("Доп. голос", "2 000 ₽.", "с₽ок: 3 дня", this.extraVoice);
      } else {
        this.service.mixContainers.push(new mixContainer("Доп. голос", "2 000 ₽.", "срок: 3 дня", this.extraVoice));
      }
    }
    if (this.online > 0) {
      if (this.selectedOnline) {
        this.service.mixContainers[this.indexOnline] = new mixContainer("Онлайн", "2 000 ₽.", "срок: до 5 дней", this.online);
      } else {
        this.service.mixContainers.push(new mixContainer("Онлайн", "2 000 ₽.", "срок: до 5 дней", this.online));
      }
    }
    if (this.fromTeacher > 0) {
      if (this.selectedFromTeacher) {
        this.service.mixContainers[this.indexFromTeacher] = new mixContainer("С педагогом", "2 000 ₽.", "срок: 3-4 дня", this.fromTeacher);
      } else {
        if (this.selectedWithoutTeacher) {
          this.service.mixContainers[this.indexWithoutTeacher] = new mixContainer("С педагогом", "2 000 ₽.", "срок: 3-4 дня", this.fromTeacher);
        } else {
          this.service.mixContainers.push(new mixContainer("С педагогом", "2 000 ₽.", "срок: 3-4 дня", this.fromTeacher));
        }
      }
    }
    if (this.withoutTeacher > 0) {
      if (this.selectedWithoutTeacher) {
        this.service.mixContainers[this.indexWithoutTeacher] = new mixContainer("Без педагога", "3 000 ₽.", "срок: 4-5 дня", this.withoutTeacher);
      } else {
        if (this.selectedFromTeacher) {
          this.service.mixContainers[this.indexFromTeacher] = new mixContainer("Без педагога", "3 000 ₽.", "срок: 4-5 дня", this.withoutTeacher);
        } else {
          this.service.mixContainers.push(new mixContainer("Без педагога", "3 000 ₽.", "срок: 4-5 дня", this.withoutTeacher));
        }
      }
    }
    localStorage.setItem(this.service.key, JSON.stringify(this.service));
    this.router.navigate(['/addition']);
  }

  upWithoutTeacher() {
    if (this.fromTeacher > 0) {
      this.fromTeacher = 0;
    }
    if (this.withoutTeacher < 30) {
      this.withoutTeacher++;
    }
  }

  downWithoutTeacher() {
    if (this.withoutTeacher > 0) {
      this.withoutTeacher--;
    }
  }

  upFromTeacher() {
    if (this.withoutTeacher > 0) {
      this.withoutTeacher = 0;
    }
    if (this.fromTeacher < 30) {
      this.fromTeacher++;
    }
  }

  downFromTeacher() {
    if (this.fromTeacher > 0) {
      this.fromTeacher--;
    }
  }

  upExtraVoice() {
    if (this.extraVoice < 30) {
      this.extraVoice++;
    }
  }

  downExtraVoice() {
    if (this.extraVoice > 0) {
      this.extraVoice--;
    }
  }

  upOnline() {
    if (this.online < 30) {
      this.online++;
    }
  }

  downOnline() {
    if (this.online > 0) {
      this.online--;
    }
  }
}
