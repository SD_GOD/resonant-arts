import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasteringComponent } from './mastering.component';

describe('MasteringComponent', () => {
  let component: MasteringComponent;
  let fixture: ComponentFixture<MasteringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasteringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasteringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
