import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {mixContainer, ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-mastering',
  templateUrl: './mastering.component.html',
  styleUrls: ['./mastering.component.css']
})
export class MasteringComponent implements OnInit, AfterViewInit {


  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }
  }

  entity = this.service.entity;
  selectedITunes = false;
  selectingITunes = false;
  selectedMastering = false;
  selectingMastering = false;
  indexITunes = 0;
  name = "";
  cost = "";
  deadline = "";
  indexMastering = 0;
  iTunes = "";
  iTunesDeadline = "";
  iTunesCost = "";

  ngOnInit() {
    this.entity = this.service.entity;
    this.service.mixContainers.forEach((container, key) => {
      if (container.name.includes("Цифровой") || container.name.includes("STEM")) {
        if (container.cost.includes("1500 ₽.") || container.cost.includes("1800 ₽.")) {
          this.name = container.name;
          this.cost = container.cost;
          this.deadline = container.deadline;
          this.indexMastering = key;
          this.selectedMastering = true;
        }
      }

    });
    this.service.mixContainers.forEach((container, key) => {
      if (container.name.includes("iTunes")) {
        this.iTunes = container.name;
        this.iTunesCost = container.cost;
        this.iTunesDeadline = container.deadline;
        this.indexITunes = key;
        this.selectedITunes = true;
      }

    })
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goMasteringOnline() {
    this.router.navigate(['/masteringOnline']);
  }

  next() {
    if (this.selectedITunes) {
      this.service.mixContainers[this.indexMastering] = new mixContainer(this.iTunes, this.iTunesCost, this.iTunesDeadline);
    } else {
      if (this.selectingITunes) {
        this.service.mixContainers.push(new mixContainer(this.iTunes, this.iTunesCost, this.iTunesDeadline));
      }
    }
    if (this.selectedMastering) {
      this.service.mixContainers[this.indexMastering] = new mixContainer(this.name, this.cost, this.deadline);
    } else {
      if (this.selectingMastering) {
        this.service.mixContainers.push(new mixContainer(this.name, this.cost, this.deadline));
      }
    }
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/masteringOnline']);
  }

  goOnline() {
    this.router.navigate(['/online']);
  }

  goAnalog() {
    this.router.navigate(['/analog']);
  }

  select(value: string) {
    this.name = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.cost = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.deadline = value.substring(0);
    this.selectingMastering = true;
  }

  check(value: string) {
    this.selectingITunes = !this.selectingITunes;
    if (this.selectingITunes) {
      this.iTunes = value.substring(0, value.indexOf(';'));
      value = value.substring(value.indexOf(';') + 1);
      this.iTunesCost = value.substring(0, value.indexOf(';'));
      value = value.substring(value.indexOf(';') + 1);
      this.iTunesDeadline = value.substring(0);
    } else {
      this.iTunes = "";
      this.iTunesCost = "";
      this.iTunesDeadline = "";
    }
  }

  digital() {
    return this.cost.includes('1500 ₽.');
  }

  stem() {
    return this.cost.includes('1800 ₽.');
  }

  checkITunes() {
    return this.iTunesCost.includes('500 ₽.');
  }


}
