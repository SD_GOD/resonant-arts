import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-format',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.css']
})
export class FormatComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }
  }

  entity = this.service.entity;
  disable = true;
  disableUrgency = true;
  disableSoundEngineer = true;
  disablePayment = true;
  format = '';

  ngOnInit() {
    this.entity = this.service.entity;
    this.disableUrgency = this.service.disableUrgency;
    this.disableSoundEngineer = this.service.disableSoundEngineer;
    this.disablePayment = this.service.disablePayment;
    this.format = this.service.format;
    if (this.format.length !== 0) {
      this.disable = false;
    }
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
    this.elementRef.nativeElement.ownerDocument.body.style.width = window.innerWidth;
    console.log(window.innerWidth);
    console.log(this.elementRef.nativeElement.ownerDocument.body.style.width);
  }

  select(value: string) {
    this.format = value;
    this.disable = false;
  }

  next() {
    this.service.format = this.format;
    this.service.disableUrgency = false;
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/urgency']);
  }

  goUrgency() {
    this.router.navigate(['/urgency']);
  }

  goSoundEngineer() {
    this.router.navigate(['/soundEngineer']);
  }

  goPayment() {
    this.router.navigate(['/payment']);
  }

  mp3() {
    return this.service.format.includes("MP3");
  }

  wav() {
    return this.service.format.includes("WAV");
  }

  multitrek() {
    return this.service.format.includes("Мультитрек");
  }

  goAddition() {
    this.router.navigate(['/addition']);
  }
}
