import {Component, DoCheck, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit, DoCheck {

  entity = this.service.entity;
  disable = true;
  artist = this.service.artist;
  project = this.service.project;
  nameSong = this.service.nameSong;
  dateRelease = this.service.dateRelease;
  artistPhone = this.service.artistPhone;
  artistEmail = this.service.artistEmail;
  incorrectEmail=false;

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));

  }

  ngOnInit() {
    this.entity = this.service.entity;
    this.disable = true;
    this.artist = this.service.artist;
    this.project = this.service.project;
    this.nameSong = this.service.nameSong;
    this.dateRelease = this.service.dateRelease;
    this.artistPhone = this.service.artistPhone;
    this.artistEmail = this.service.artistEmail;
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  changeEntity() {
    this.entity = !this.entity;
  }

  next() {
    const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(this.artistEmail) === false) {
      this.incorrectEmail=true;
      return;
    }
    this.service.entity = this.entity;
    this.service.artist = this.artist;
    this.service.project = this.project;
    this.service.nameSong = this.nameSong;
    this.service.dateRelease = this.dateRelease;
    this.service.artistPhone = this.artistPhone;
    this.service.artistEmail = this.artistEmail;
    localStorage.setItem(this.service.key, JSON.stringify(this.service));
    this.router.navigate(['/mixing']);
  }

  enterArtist(value: string) {
    this.artist = value;
  }

  ngDoCheck() {
    this.disable = !(this.artist.length && this.project.length && this.nameSong.length && this.dateRelease.length
      && this.artistPhone.length && this.artistEmail.length);

  }

  enterProject(value: string) {
    this.project = value;
  }

  enterNameSong(value: string) {
    this.nameSong = value;
  }

  enterDate(value: string) {
    this.dateRelease = value;
  }

  enterArtistPhone(value: string) {
    this.artistPhone = value;
  }

  enterArtistEmail(value: string) {
    this.artistEmail = value;
    this.incorrectEmail = false;
  }

}
