import {Component, OnInit} from '@angular/core';
import {ServicesComponent} from "../../services/finalPages.services";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-burger-menu',
  templateUrl: './burger-menu.component.html',
  styleUrls: ['./burger-menu.component.css']
})
export class BurgerMenuComponent implements OnInit {
  active = false;
  entity = this.service.entity;
  disableAll = true;
  disableUrgency = true;
  disableSoundEngineer = true;
  disablePayment = true;
  disableRecommendationStudio = true;
  disableRecommendationArtist = true;

  constructor(private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    this.service = JSON.parse(localStorage.getItem(this.service.key));
  }

  ngOnInit() {
    this.entity = this.service.entity;
    this.disableAll = this.service.mixContainers[0].name.length === 0;
    this.disableRecommendationStudio = this.service.recommendationStudio.length === 0;
    this.disableRecommendationArtist = this.service.recommendationArtist.length === 0;
    this.disableUrgency = this.service.disableUrgency;
    this.disableSoundEngineer = this.service.disableSoundEngineer;
    this.disablePayment = this.service.disablePayment;
  }

  onBurgerClicked() {
    this.active = !this.active;
  }

  onMixing() {
    this.router.navigate(['/mixing']);
  }

  onMastering() {
    this.router.navigate(['/mastering']);
  }

  onTuning() {
    this.router.navigate(['/tuning']);
  }

  onUrgency() {
    this.router.navigate(['/urgency']);
  }

  onFormat() {
    this.router.navigate(['/format']);
  }

  onAddition() {
    this.router.navigate(['/addition']);
  }

  onSoundEngineer() {
    this.router.navigate(['/soundEngineer']);
  }

  onPayment() {
    this.router.navigate(['/payment']);
  }

  onRecommendation() {
    this.router.navigate(['/recommendation']);
  }

  onWishes() {
    this.router.navigate(['/wishes']);
  }
}
