import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";


@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined || this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.total = 0;
    this.totalSumUrgency = 0;
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if (this.service.artist.length === 0) {
      this.router.navigate(['/contacts']);
      return;
    }
    if (this.service.mixContainers[0].name.length === 0) {
      this.router.navigate(['/mixing']);
      return;
    }
    if (this.service.format.length === 0) {
      this.router.navigate(['/format']);
      return;
    }
    if (this.service.soundEngineer.length === 0) {
      this.router.navigate(['/soundEngineer']);
      return;
    }
    if (this.service.payment.length === 0) {
      this.router.navigate(['/payment']);
      return;
    }
    if (this.service.recommendationStudio.length === 0) {
      this.router.navigate(['/recommendation']);
      return;
    }
    if (this.service.recommendationArtist.length === 0) {
      this.router.navigate(['/wishes']);
      return;
    }
  }

  entity = false;
  mixContainers: any;
  total: number;
  urgency: number;
  totalSumUrgency: number;
  count = 0;
  countUrgency = 0;
  costUrgency = [];

  ngOnInit() {
    this.entity = this.service.entity;
    this.mixContainers = this.service.mixContainers;
    this.urgency = this.service.urgency;
  }


  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goWishes() {
    this.router.navigate(['/wishes']);
  }

  goFinal() {
    this.router.navigate(['/final']);
  }

  sum(count: number, cost: string) {
    if (this.count < this.mixContainers.length) {
      cost = cost.substring(0, cost.indexOf("₽"));
      let costNum = parseInt(cost.substring(0, cost.indexOf(" ")) + cost.substring(cost.indexOf(" ") + 1), 10);
      if (count === undefined) {
        count = 1;
      }
      let func = (costNum * count);
      this.total += costNum * count;
      this.count++;
      return func;
    } else {
      this.service.totalCost = this.total;
      this.total = 0;
      this.count = 0;
    }
  }

  sumUrgency(count: number, cost: string) {
    if (this.countUrgency < this.mixContainers.length) {
      cost = cost.substring(0, cost.indexOf("₽"));
      let costNum = parseInt(cost.substring(0, cost.indexOf(" ")) + cost.substring(cost.indexOf(" ") + 1), 10);
      if (count === undefined) {
        count = 1;
      }
      this.countUrgency++;
      this.total += costNum * count;
      this.costUrgency.push(costNum * count);
      return (costNum * count);
    } else {
      this.service.totalCost = this.totalSumUrgency
      this.total = 0;
      this.totalSumUrgency = 0;
      this.countUrgency = 0;
    }
  }

  totalUrgency(i: number, urgency: number) {
    let func: any;
    if (urgency === 0 || urgency === 1) {
      func = (this.costUrgency[i] * 2).toString();
      this.totalSumUrgency += this.costUrgency[i] * 2;
    }
    if (urgency === 2) {
      func = (this.costUrgency[i] * 1.7).toString();
      this.totalSumUrgency += this.costUrgency[i] * 1.7;
    }
    if (urgency === 3) {
      func = (this.costUrgency[i] * 1.5).toString();
      this.totalSumUrgency += this.costUrgency[i] * 1.5;
    }
    return func;
  }

  exitNumber(cost: number) {
    let res= cost.toString();
    if(res.length>6){
      res=res.substring(0,res.length-6)+" "+ res.substring(res.length-6)+res.substring(0,res.length-3)+" "+ res.substring(res.length-3);
    }
    if(res.length>3){
      res=res.substring(0,res.length-3)+" "+ res.substring(res.length-3);
    }
    return res;
  }

  next() {
    localStorage.setItem(this.service.key, JSON.stringify(this.service));
    this.router.navigate(['/final']);
  }
}
