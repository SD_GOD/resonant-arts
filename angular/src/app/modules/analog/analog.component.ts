import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {mixContainer, ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-analog',
  templateUrl: './analog.component.html',
  styleUrls: ['./analog.component.css']
})
export class AnalogComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if (this.service.artist.length === 0) {
      this.router.navigate(['/contacts']);
      return;
    }
    if (this.service.mixContainers[0].name.length === 0) {
      this.router.navigate(['/mixing']);
      return;
    }
  }

  entity: boolean;
  selectedAnalog = false;
  selectingAnalog = false;
  name = "";
  cost = "";
  deadline = "";
  indexAnalog = 0;

  ngOnInit() {
    this.entity = this.service.entity;
    this.service.mixContainers.forEach((container, key) => {
      if (container.name.includes("Wim Bult") || container.name.includes("Kramer") ||
        container.name.includes("Dave Blackman") || container.name.includes("John Webber")) {
        this.selectedAnalog = true;
        this.indexAnalog = key;
        this.name = container.name;
        this.cost = container.cost;
        this.deadline = container.deadline;
      }
    });
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goTuning() {
    this.router.navigate(['/tuning']);
  }

  goMasteringOnline() {
    this.router.navigate(['/masteringOnline']);
  }

  goMastering() {
    this.router.navigate(['/mastering']);
  }

  next() {
    if (this.selectingAnalog && this.selectedAnalog) {
      this.service.mixContainers[this.indexAnalog] = new mixContainer(this.name, this.cost, this.deadline);
    } else {
      if (this.selectingAnalog) {
        this.service.mixContainers.push(new mixContainer(this.name, this.cost, this.deadline));
      }
    }
    localStorage.setItem(this.service.key, JSON.stringify(this.service));
    this.router.navigate(['/tuning']);
  }

  select(value: string) {
    this.name = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.cost = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.deadline = value.substring(0);
    this.selectingAnalog = true;
  }

  john() {
    return this.name.includes("John Webber")
  }

  dave() {
    return this.name.includes("Dave Blackman")
  }

  kramer() {
    return this.name.includes("Kramer")
  }

  wim() {
    return this.name.includes("Wim Bult")
  }
}
