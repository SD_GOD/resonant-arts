import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.css']
})
export class RecommendationComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }
    if( this.service.format.length === 0){
      this.router.navigate(['/format']);
      return;
    }
    if( this.service.soundEngineer.length === 0){
      this.router.navigate(['/soundEngineer']);
      return;
    }
    if( this.service.payment.length === 0){
      this.router.navigate(['/payment']);
      return;
    }
  }

  entity: boolean;
  disable = true;
  recommendationStudio = "";

  ngOnInit() {
    this.entity = this.service.entity;
    this.recommendationStudio = this.service.recommendationStudio;
    if (this.recommendationStudio.length > 0) {
      this.disable = false;
    }
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goPayment() {
    this.router.navigate(['/payment']);
  }

  goWishes() {
    this.router.navigate(['/wishes']);
  }

  next() {
    this.service.recommendationStudio = this.recommendationStudio;
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/wishes']);
  }

  text(value: any) {
    this.recommendationStudio = value;
    this.disable = false;
  }

}
