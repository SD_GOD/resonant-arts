import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {mixContainer, ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-addition',
  templateUrl: './addition.component.html',
  styleUrls: ['./addition.component.css']
})
export class AdditionComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));

    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }

  }

  entity: boolean;
  tunecore = 0;
  selectedTunecore: boolean;
  costTunecore: string;
  nameTunecore: string;
  indexTunecore: number;
  revision = 0;
  selectedRevision: boolean;
  costRevision: string;
  nameRevision: string;
  indexRevision: number;
  backingVocal = 0;
  selectedBackingVocal: boolean;
  costBackingVocal: string;
  nameBackingVocal: string;
  indexBackingVocal: number;
  soundDesign = 0;
  selectedSoundDesign: boolean;
  costSoundDesign: string;
  nameSoundDesign: string;
  indexSoundDesign: number;
  revisionVoiceActing = 0;
  selectedRevisionVoiceActing: boolean;
  costRevisionVoiceActing: string;
  nameRevisionVoiceActing: string;
  indexRevisionVoiceActing: number;
  backingVocalParty = 0;
  selectedBackingVocalParty: boolean;
  costBackingVocalParty: string;
  nameBackingVocalParty: string;
  indexBackingVocalParty: number;

  ngOnInit() {
    this.entity=this.service.entity;
    this.service.mixContainers.forEach((container, key) => {
      if (container.name.includes("Tunecore")) {
        this.selectedTunecore = true;
        this.indexTunecore = key;
        this.nameTunecore = container.name;
        this.costTunecore = container.cost;
        this.tunecore = container.count;
      }
      if (container.name.includes("Редакция (музыка)")) {
        this.selectedRevision = true;
        this.indexRevision = key;
        this.nameRevision = container.name;
        this.costRevision = container.cost;
        this.revision = container.count;
      }
      if (container.name.includes("Бэк-вокал (вся песня)")) {
        this.selectedBackingVocal = true;
        this.indexBackingVocal = key;
        this.nameBackingVocal = container.name;
        this.costBackingVocal = container.cost;
        this.backingVocal = container.count;
      }
      if (container.name.includes("Саунд-дизайн")) {
        this.selectedSoundDesign = true;
        this.indexSoundDesign = key;
        this.nameSoundDesign = container.name;
        this.costSoundDesign = container.cost;
        this.soundDesign = container.count;
      }
      if (container.name.includes("Редакция (озвучка)")) {
        this.selectedRevisionVoiceActing = true;
        this.indexRevisionVoiceActing = key;
        this.nameRevisionVoiceActing = container.name;
        this.costRevisionVoiceActing = container.cost;
        this.revisionVoiceActing = container.count;
      }
      if (container.name.includes("Бэк-вокал (партия)")) {
        this.selectedBackingVocalParty = true;
        this.indexBackingVocalParty = key;
        this.nameBackingVocalParty = container.name;
        this.costBackingVocalParty = container.cost;
        this.backingVocalParty = container.count;
      }
    });
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goTuning() {
    this.router.navigate(['/tuning']);
  }

  goFormat() {
    this.router.navigate(['/format']);
  }

  next() {
    if (this.tunecore > 0) {
      if (this.selectedTunecore) {
        this.service.mixContainers[this.indexTunecore] = new mixContainer("Tunecore", "1 500 ₽.", null, this.tunecore);
      } else {
        this.service.mixContainers.push(new mixContainer("Tunecore", "1500 ₽.", null, this.tunecore));
      }
    }
    if (this.backingVocalParty > 0) {
      if (this.selectedBackingVocalParty) {
        this.service.mixContainers[this.indexBackingVocalParty] = new mixContainer("Бэк-вокал (партия)", "3 000 ₽.", null, this.backingVocalParty);
      } else {
        this.service.mixContainers.push(new mixContainer("Бэк-вокал (партия)", "3 000 ₽.", null, this.backingVocalParty));
      }
    }
    if (this.backingVocal > 0) {
      if (this.selectedBackingVocal) {
        this.service.mixContainers[this.indexBackingVocal] = new mixContainer("Бэк-вокал (вся песня)", "5 000 ₽.", null, this.backingVocal);
      } else {
        this.service.mixContainers.push(new mixContainer("Бэк-вокал (вся песня)", "5 000 ₽.", null, this.backingVocal));
      }
    }
    if (this.soundDesign > 0) {
      if (this.selectedSoundDesign) {
        this.service.mixContainers[this.indexSoundDesign] = new mixContainer("Саунд-дизайн", "2 000 ₽.", null, this.soundDesign);
      } else {
        this.service.mixContainers.push(new mixContainer("Саунд-дизайн", "2 000 ₽.", null, this.soundDesign));
      }
    }
    if (this.revisionVoiceActing > 0) {
      if (this.selectedRevisionVoiceActing) {
        this.service.mixContainers[this.indexRevisionVoiceActing] = new mixContainer("Редакция (озвучка)", "3 500 ₽.", null, this.revisionVoiceActing);
      } else {
        this.service.mixContainers.push(new mixContainer("Редакция (озвучка)", "3 500 ₽.", null, this.revisionVoiceActing));
      }
    }
    if (this.revision > 0) {
      if (this.selectedRevision) {
        this.service.mixContainers[this.indexRevision] = new mixContainer("Редакция (музыка)", "2 000 ₽.", null, this.revision);
      } else {
        this.service.mixContainers.push(new mixContainer("Редакция (музыка)", "2 000 ₽.", null, this.revision));
      }
    }
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/format']);
  }

  upTunecore() {
    if (this.tunecore < 10) {
      this.tunecore++
    }
  }

  downTunecore() {
    if (this.tunecore > 0) {
      this.tunecore--;
    }
  }

  upRevision() {
    if (this.revision < 50) {
      this.revision++
    }
  }

  downRevision() {
    if (this.revision > 0) {
      this.revision--;
    }
  }

  upBackingVocal() {
    if (this.backingVocal < 10) {
      this.backingVocal++
    }
  }

  downBackingVocal() {
    if (this.backingVocal > 0) {
      this.backingVocal--;
    }
  }

  upSoundDesign() {
    if (this.soundDesign < 50) {
      this.soundDesign++
    }
  }

  downSoundDesign() {
    if (this.soundDesign > 0) {
      this.soundDesign--;
    }
  }

  upRevisionVoiceActing() {
    if (this.revisionVoiceActing < 30) {
      this.revisionVoiceActing++
    }
  }

  downRevisionVoiceActing() {
    if (this.revisionVoiceActing > 0) {
      this.revisionVoiceActing--;
    }
  }

  upBackingVocalParty() {
    if (this.backingVocalParty < 10) {
      this.backingVocalParty++
    }
  }

  downBackingVocalParty() {
    if (this.backingVocalParty > 0) {
      this.backingVocalParty--;
    }
  }
}
