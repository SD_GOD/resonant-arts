import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-wishes',
  templateUrl: './wishes.component.html',
  styleUrls: ['./wishes.component.css']
})
export class WishesComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));

    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }
    if( this.service.format.length === 0){
      this.router.navigate(['/format']);
      return;
    }

    if( this.service.soundEngineer.length === 0){
      this.router.navigate(['/soundEngineer']);
      return;
    }
    if( this.service.payment.length === 0){
      this.router.navigate(['/payment']);
      return;
    }
    if( this.service.recommendationStudio.length === 0){
      this.router.navigate(['/recommendation']);
      return;
    }
  }

  entity: boolean;
  disable = true;
  recommendationArtist = "";

  ngOnInit() {
    this.entity = this.service.entity;
    this.recommendationArtist = this.service.recommendationArtist;
    if (this.recommendationArtist.length > 0) {
      this.disable = false;
    }
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goRecommendation() {
    this.router.navigate(['/recommendation']);
  }

  next() {
    this.service.recommendationArtist = this.recommendationArtist;
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/total']);
  }

  text(value: any) {
    this.recommendationArtist = value;
    this.disable = false;
  }
}
