import {AfterViewInit, Component, DoCheck, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {mixContainer, ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-online',
  templateUrl: './online.component.html',
  styleUrls: ['./online.component.css']
})
export class OnlineComponent implements OnInit, AfterViewInit, DoCheck {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if (this.service.artist.length === 0) {
      this.router.navigate(['/contacts']);
      return;
    }
  }

  entity: boolean;
  disable = true;
  name = this.service.mixContainers[0].name;
  cost = this.service.mixContainers[0].cost;
  deadline = this.service.mixContainers[0].deadline;

  ngOnInit() {
    this.entity = this.service.entity;
    this.name = this.service.mixContainers[0].name;
    this.cost = this.service.mixContainers[0].cost;
    this.deadline = this.service.mixContainers[0].deadline;
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goMixing() {
    this.router.navigate(['/mixing']);
  }

  goMastering() {
    this.service.mixContainers[0] = new mixContainer(this.name, this.cost, this.deadline);
    localStorage.setItem(this.service.key, JSON.stringify(this.service));
    this.router.navigate(['/mastering']);
  }

  select(value: string) {
    this.name = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.cost = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.deadline = value.substring(0);
  }

  ngDoCheck() {
    if (this.service.mixContainers[0].name.length === 0) {
      this.disable = !this.name.length;
    } else {
      this.disable = !this.service.mixContainers[0].name.length;
    }
  }

  basic() {
    return this.service.mixContainers[0].cost.includes('4000 Р.');
  }

  secondLevel() {
    return this.service.mixContainers[0].cost.includes('8000 Р.');
  }

  firstLevel() {
    return this.service.mixContainers[0].cost.includes('14000 Р.');
  }
}
