import {AfterViewInit, Component, ElementRef, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {mixContainer, ServicesComponent} from "../services/finalPages.services";
import {container} from "@angular/core/src/render3";

@Component({
  selector: 'app-mixing',
  templateUrl: './mixing.component.html',
  styleUrls: ['./mixing.component.css']
})
export class MixingComponent implements OnInit, AfterViewInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
  }

  entity: boolean;
  name = this.service.mixContainers[0].name;
  cost = this.service.mixContainers[0].cost;
  deadline = this.service.mixContainers[0].deadline;

  ngOnInit() {
    this.entity = this.service.entity;
    this.name = this.service.mixContainers[0].name;
    this.cost = this.service.mixContainers[0].cost;
    this.deadline = this.service.mixContainers[0].deadline;
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  onOnline() {
    this.service.mixContainers[0] = new mixContainer(this.name, this.cost, this.deadline);
    /*alert(this.service.mixContainers[0]);*/
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/online']);
  }

  onContacts() {
    this.router.navigate(['/contacts']);
  }

  select(value: string) {
    this.name = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.cost = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.deadline = value.substring(0);
    /*alert(this.name);*/
  }

  presentation() {
    return this.service.mixContainers[0].cost.includes('3000 ₽.');
  }

  basic() {
    return this.service.mixContainers[0].cost.includes('5000 ₽.');
  }

  secondLevel() {
    return this.service.mixContainers[0].cost.includes('10000 ₽.');
  }

  firstLevel() {
    return this.service.mixContainers[0].cost.includes('16000 ₽.');
  }
}
