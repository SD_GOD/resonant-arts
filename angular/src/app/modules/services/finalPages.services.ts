export class ServicesComponent {
  entity = false;
  emailManager = "";
  artist = "";
  project = "";
  nameSong = "";
  dateRelease = "";
  artistPhone = "";
  artistEmail = "";
  disableUrgency = true;
  disableSoundEngineer = true;
  disablePayment = true;
  format = "";
  urgency = null;
  payment = "";
  recommendationStudio = "";
  recommendationArtist = "";
  mixContainers = [new mixContainer("", "", "")];
  soundEngineer = "";
  totalCost: number;
  key: string;
  photo: string;
  constructor() {
    this.entity = false;
    this.emailManager = "";
    this.artist = "";
    this.project = "";
    this.nameSong = "";
    this.dateRelease = "";
    this.artistPhone = "";
    this.artistEmail = "";
    this.disableUrgency = true;
    this.disableSoundEngineer = true;
    this.disablePayment = true;
    this.format = "";
    this.urgency = null;
    this.payment = "";
    this.recommendationStudio = "";
    this.recommendationArtist = "";
    this.mixContainers = [new mixContainer("", "", "")];
    this.soundEngineer = "";
    this.totalCost = 0;
    this.key = "";
    this.photo = "";
  }
}

export class mixContainer {
  name: string;
  cost: string;
  deadline: string;
  count = 0;

  constructor(name: string, cost: string, deadline?: string, count?: number) {
    this.name = name;
    this.cost = cost;
    this.deadline = deadline;
    this.count = count;
  }
}
