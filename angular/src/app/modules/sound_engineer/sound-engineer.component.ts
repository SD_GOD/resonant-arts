import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-sound-engineer',
  templateUrl: './sound-engineer.component.html',
  styleUrls: ['./sound-engineer.component.css']
})
export class SoundEngineerComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }
    if( this.service.format.length === 0){
      this.router.navigate(['/format']);
      return;
    }
  }

  entity: boolean;
  disable = true;
  disablePayment = true;
  soundEngineer = "";

  ngOnInit() {
    this.entity = this.service.entity;
    this.disablePayment = this.service.disablePayment;
    this.soundEngineer = this.service.soundEngineer;
    if (this.soundEngineer.length !== 0) {
      this.disable = false;
    }
  }

  select(value: string) {
    this.soundEngineer = value;
    this.disable = false;
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goPayment() {
    this.router.navigate(['/payment']);
  }

  goFormat() {
    this.router.navigate(['/format']);
  }

  goUrgency() {
    this.router.navigate(['/urgency']);
  }

  next() {
    this.service.soundEngineer = this.soundEngineer;
    this.service.disableSoundEngineer = false;
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/payment']);
  }

  sasha() {
    return this.service.soundEngineer.includes("Саша П.");
  }

  dasha() {
    return this.service.soundEngineer.includes("Даша П.");
  }

  grisha() {
    return this.service.soundEngineer.includes("Гриша Ч.");
  }

  stas() {
    return this.service.soundEngineer.includes("Стас К.");
  }
}
