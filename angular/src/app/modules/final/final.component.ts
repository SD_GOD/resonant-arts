import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";
import * as jsPDF from "jspdf";
import html2canvas from "html2canvas";
import axios from 'axios';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.css']
})
export class FinalComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private readonly service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined || this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if (this.service.artist.length === 0) {
      this.router.navigate(['/contacts']);
      return;
    }
    if (this.service.mixContainers[0].name.length === 0) {
      this.router.navigate(['/mixing']);
      return;
    }
    if (this.service.format.length === 0) {
      this.router.navigate(['/format']);
      return;
    }
    if (this.service.soundEngineer.length === 0) {
      this.router.navigate(['/soundEngineer']);
      return;
    }
    if (this.service.payment.length === 0) {
      this.router.navigate(['/payment']);
      return;
    }
    if (this.service.recommendationStudio.length === 0) {
      this.router.navigate(['/recommendation']);
      return;
    }
    if (this.service.recommendationArtist.length === 0) {
      this.router.navigate(['/wishes']);
      return;
    }
  }

  entity = this.service.entity;
  mixContainers = this.service.mixContainers;
  manager = "Мария Гольм";
  soundEngineer = this.service.soundEngineer;
  totalCost = this.service.totalCost;
  format = this.service.format;
  payment = this.service.payment;
  todayDate = "20.05.19г.";
  reference = "http://localhost:4200/final";
  artist = this.service.artist;
  project = this.service.project;
  nameSong = this.service.nameSong;
  dateRelease = this.service.dateRelease;
  artistPhone = this.service.artistPhone;
  artistEmail = this.service.artistEmail;
  recommendations = this.service.recommendationStudio;
  wishes = this.service.recommendationArtist;

  /*public captureScreen()
  {
    var data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 400;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('MYPdf.pdf'); // Generated PDF
    });
  }*/


  captureScreen() {
    // parentdiv is the html element which has to be converted to PDF
    html2canvas(document.querySelector("html")).then(canvas => {

      // Few necessary setting options
      var imgWidth = screen.width;
      /*var pageHeight = 400;*/
      var imgHeight = screen.height; /*canvas.height * imgWidth / canvas.width;*/
      /*var heightLeft = imgHeight;*/

      const image = canvas.toDataURL('image/png').replace("image/png", "image/octet-stream");
      /* window.location.href = image; // it will save locally*/
      var respng = image;
      respng = respng.substring(respng.indexOf(",") + 1);
      console.log(respng);
      this.service.photo = respng;
      /*let pdf = new jsPDF('l', 'pt', [screen.width,screen.height]/!*[850,427]*!/);//('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);*/

      /*
       var respng = {png: contentDataURL};
       respng.png=respng.png.substring(respng.png.indexOf(",") + 1);
       console.log(contentDataURL);
       axios.post('http://tz.resonantarts.com/pdf.php', JSON.stringify(respng));
       let pdf = new jsPDF('l', 'mm', 'a4'); // A4 size page of PDF
       var position = 0;
       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
       var res = {pdf: pdf.output('datauristring')};
       res.pdf = res.pdf.substring(res.pdf.indexOf(",") + 1);
       //await axios.post('http://tz.resonantarts.com/pdf.php', JSON.stringify(res));

      var blob = pdf.output('blob');
      var fd = new FormData();
      fd.append('data', blob);
      $.ajax({
        type: 'POST',
        url: 'http://tz.resonantarts.com/info.php',
        data: fd,
        processData: false,
        contentType: false
      }).done(function(data) {
        console.log(data);
      });*/

      /*  pdf.output('save', 'C:/Users/106/Downloads'); 'D:/OTHERS/Работа/resonant-arts/angular/src/assets/MYPdf.pdf');*/ // Generated PDF
      localStorage.setItem(this.service.key, JSON.stringify(this.service));
      this.router.navigate(['/sending']);
    });
  }

  ngOnInit() {
    this.entity = this.service.entity;
    this.mixContainers = this.service.mixContainers;
    this.manager = "Мария Гольм";
    this.soundEngineer = this.service.soundEngineer;
    this.totalCost = this.service.totalCost;
    this.format = this.service.format;
    this.payment = this.service.payment;
    this.todayDate = "20.05.19г.";
    this.reference = "http://localhost:4200/final";
    this.artist = this.service.artist;
    this.project = this.service.project;
    this.nameSong = this.service.nameSong;
    this.dateRelease = this.service.dateRelease;
    this.artistPhone = this.service.artistPhone;
    this.artistEmail = this.service.artistEmail;
    this.recommendations = this.service.recommendationStudio;
    this.wishes = this.service.recommendationArtist;
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  exitNumber(cost: number) {
    let res = cost.toString();
    if (res.length > 6) {
      res = res.substring(0, res.length - 6) + " " + res.substring(res.length - 6) + res.substring(0, res.length - 3) + " " + res.substring(res.length - 3);
    }
    if (res.length > 3) {
      res = res.substring(0, res.length - 3) + " " + res.substring(res.length - 3);
    }
    return res;
  }

  goSending() {
    this.router.navigate(['/sending']);
  }

  enterReference(value: any) {
    this.reference= value;
  }
}
