import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-urgency',
  templateUrl: './urgency.component.html',
  styleUrls: ['./urgency.component.css']
})
export class UrgencyComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }
    if( this.service.format.length === 0){
      this.router.navigate(['/format']);
      return;
    }
  }

  entity: boolean;
  disable = true;
  disableSoundEngineer = true;
  disablePayment = true;
  urgency = null;

  ngOnInit() {
    this.entity = this.service.entity;
    this.urgency = this.service.urgency;
    this.disableSoundEngineer = this.service.disableSoundEngineer;
    this.disablePayment = this.service.disablePayment;
    if (this.urgency !== null) {
      this.disable = false;
    }
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goSoundEngineer() {
    this.router.navigate(['/soundEngineer']);
  }

  goPayment() {
    this.router.navigate(['/payment']);
  }

  select(value: number) {
    this.urgency = value;
    this.disable = false;
  }

  next() {
    this.service.urgency = this.urgency;
    this.service.disableUrgency = false;
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/soundEngineer']);
  }

  zero() {
    return this.service.urgency === 0
  }

  one() {
    return this.service.urgency === 1;
  }

  two() {
    return this.service.urgency === 2;
  }

  three() {
    return this.service.urgency === 3;
  }

  goFormat() {
    this.router.navigate(['/format']);
  }
}
