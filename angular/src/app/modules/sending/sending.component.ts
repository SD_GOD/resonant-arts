import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";
import axios from 'axios';

@Component({
  selector: 'app-sending',
  templateUrl: './sending.component.html',
  styleUrls: ['./sending.component.css']
})
export class SendingComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
  }

  emailManager = this.service.emailManager;
  soundEngineerEmail = "";
  artistEmail = this.service.artistEmail;
  entity = this.service.entity;
  photo = this.service.photo;

  sendEmail() {
    const body = {
      email: this.emailManager + ", "+ this.soundEngineerEmail + ", "+this.artistEmail ,
      png: this.photo,
      namePhoto: "PA"
    };
    console.log(body);
    axios.post('http://tz.resonantarts.com/sendpdf.php', JSON.stringify(body));
    this.router.navigate(['/repeat']);
  };

  ngOnInit() {
     this.entity = this.service.entity;
    this.emailManager = this.service.emailManager;
    this.soundEngineerEmail = "";
    this.artistEmail = this.service.artistEmail;
    this.photo = this.service.photo;
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goRepeat() {
    this.router.navigate(['/repeat']);
  }

  goFinal() {
    this.router.navigate(['/final']);
  }

  enterSoundEngineer(value: any) {
    this.soundEngineerEmail = value;
  }
}
