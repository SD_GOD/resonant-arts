import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service: ServicesComponent) {
    this.service.key = localStorage.getItem('key');
    if (this.service.key === undefined || this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if (this.service.artist.length === 0) {
      this.router.navigate(['/contacts']);
      return;
    }
    if (this.service.mixContainers[0].name.length === 0) {
      this.router.navigate(['/mixing']);
      return;
    }
    if (this.service.format.length === 0) {
      this.router.navigate(['/format']);
      return;
    }

    if (this.service.soundEngineer.length === 0) {
      this.router.navigate(['/soundEngineer']);
      return;
    }
  }

  entity: boolean;
  disable = true;
  payment = "";

  ngOnInit() {
    this.entity = this.service.entity;
    this.payment = this.service.payment;
    if (this.payment.length !== 0) {
      this.disable = false;
    }
  }

  select(value: string) {
    this.payment = value;
    this.disable = false;
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(25, 36, 47)';
  }

  goFormat() {
    this.router.navigate(['/format']);
  }

  goSoundEngineer() {
    this.router.navigate(['/soundEngineer']);
  }

  goUrgency() {
    this.router.navigate(['/urgency']);
  }

  goRecommendation() {
    this.router.navigate(['/recommendation'])
  }

  next() {
    this.service.payment = this.payment;
    this.service.disablePayment = false;
    localStorage.setItem(this.service.key, JSON.stringify(this.service));
    this.router.navigate(['/recommendation']);
  }

  selectSberbank() {
    return this.payment.includes("Сбербанк");
  }

  selectCash() {
    return this.payment.includes("Наличные");
  }

  sberbank() {
    return this.service.payment.includes("Сбербанк");
  }

  cash() {
    return this.service.payment.includes("Наличные");
  }
}
