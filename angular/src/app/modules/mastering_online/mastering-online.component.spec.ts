import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasteringOnlineComponent } from './mastering-online.component';

describe('MasteringOnlineComponent', () => {
  let component: MasteringOnlineComponent;
  let fixture: ComponentFixture<MasteringOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasteringOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasteringOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
