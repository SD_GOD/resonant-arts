import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {mixContainer, ServicesComponent} from "../services/finalPages.services";

@Component({
  selector: 'app-mastering-online',
  templateUrl: './mastering-online.component.html',
  styleUrls: ['./mastering-online.component.css']
})
export class MasteringOnlineComponent implements OnInit {

  constructor(private elementRef: ElementRef,
              private route: ActivatedRoute,
              private router: Router, private service:ServicesComponent ) {
    this.service.key=localStorage.getItem('key');
    if (this.service.key === undefined||this.service.key === null) {
      this.router.navigate(['/']);
      return;
    }
    this.service = JSON.parse(localStorage.getItem(this.service.key));
    if(this.service.artist.length === 0){
      this.router.navigate(['/contacts']);
      return;
    }
    if( this.service.mixContainers[0].name.length === 0){
      this.router.navigate(['/mixing']);
      return;
    }
  }

  entity = this.service.entity;
  selectedMastering = false;
  selectingMastering = false;
  name = "";
  cost = "";
  deadline = "";
  indexMastering = 0;

  ngOnInit() {
    this.entity= this.service.entity;
    this.service.mixContainers.forEach((container, key) => {
      if (container.name.includes("Цифровой") || container.name.includes("STEM")) {
        this.selectedMastering = true;
        this.indexMastering = key;
        if (container.cost.includes("1000 ₽.") || container.cost.includes("1400 ₽.")) {
          this.name = container.name;
          this.cost = container.cost;
          this.deadline = container.deadline;
        }
      }
    });
  }

  ngAfterViewInit(){
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor =  'rgb(25, 36, 47)';
  }

  goMastering() {
    this.router.navigate(['/mastering']);
  }

  goAnalog() {
    this.router.navigate(['/analog']);
  }

  next(){
    if (this.selectingMastering&&this.selectedMastering) { this.service.mixContainers[this.indexMastering] = new mixContainer(this.name, this.cost, this.deadline);}
    else {
      if (this.selectingMastering) {
        this.service.mixContainers.push(new mixContainer(this.name, this.cost, this.deadline));
      }
    }
    localStorage.setItem(this.service.key,JSON.stringify(this.service));
    this.router.navigate(['/analog']);
  }

  select(value: string) {
    this.name = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.cost = value.substring(0, value.indexOf(';'));
    value = value.substring(value.indexOf(';') + 1);
    this.deadline = value.substring(0);
    this.selectingMastering = true;
  }

  digital() {
    return this.cost.includes('1000 ₽.');
  }

  stem() {
    return this.cost.includes('1400 ₽.');
  }

}
